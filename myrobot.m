clear all;
clc;

startup_rvc;
l(1)=Link([0 1 0 -pi/2 1]);
l(2)=Link([pi/2 1 0 pi/2 1]);
l(3)=Link([pi/2 1 0 pi/2 1]);
l(4)=Link([0 1 0 0 1]);
r=SerialLink(l);

t=[0:1:1];  
n=50;         
m=50;         
s=1;          
f=1;

img = imread('Al-Baath_University_logo.png');          
img = imresize(img,[n m]);    

hold on
axis([-100,400,-100,400,-4,4]);

for i=1:s:n  
    
       if f==1  
           for j=1:s:m-s
               pix = impixel(img,i,j); 
               q=jtraj( [1 i j 1] , [1 i j+s 1], t );
               plot3( j  ,  i  ,  0   , '.' , 'Color' , pix./255 )
               r.plot(q); 
           end
           plot3( m  ,  i  ,  0   , '.' , 'Color' , pix./255 )
           
       else
           for j=m:-s:1+s
               pix = impixel(img,i,j);
               q=jtraj( [1 i j 1] , [1 i j-s 1] , t );
               plot3( j  ,  i  ,  0    , '.' , 'Color' , pix./255 )
               r.plot(q); 
           end
           plot3( 1  ,  i  ,  0    , '.' , 'Color' , pix./255 )
           
       end    
    
       if i ~= n
           if f==1
               pix = impixel(img,i+s,m);
               q=jtraj( [1 i m 1 ] , [1 i+s m 1] , t );
               r.plot(q);
               plot3( m  ,  i+s  ,  0    , '.' , 'Color' , pix./255 )
               f=0;
           else
               pix = impixel(img,i+s,1);
               q=jtraj( [1 i 1 1 ] , [1 i+s 1 1] , t );
               r.plot(q);
               plot3( 1  ,  i+s  ,  0    , '.' , 'Color' , pix./255 )
               f=1;
           end 
       end   
end    