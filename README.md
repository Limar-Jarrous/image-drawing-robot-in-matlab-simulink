# Image-Drawing-Robot-in-Matlab-Simulink
This project is done by using Matlab Simulink with Robotics Toolbox library

To import the the library just use *startup_rvc* in your code. It will run the 'startup_rvc.m' file.

To see an example of code, open 'Circle.m' file. And you can run this file to see a robot draws a circle in space.
For a full discription of the Robotics Toolbox library, read the 'Robotics Toolbox for MATLAB.pdf'

Now, run the 'myrobot.m' file to see the robot drawing any image you want after putting the image in the same folder with files or you can write the full path of the image in the code.
