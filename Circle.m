clear all;
clc;

rad=2;

startup_rvc
l(1)=Link([0 1 0 -pi/2 0]);
l(2)=Link([0 rad 0 -pi/2 1]);
l(3)=Link([0 1 0   0   0]);
r=SerialLink(l);

t=[0:1];

for theta=0:pi/100:(2*pi)-(pi/100)

q1=[theta rad 0];
q2=[theta+pi/100 rad 0];
q=jtraj(q1,q2,t);
r.plot(q)
hold on
plot3(rad*cos(theta+pi/2) , rad*sin(theta+pi/2), 0*theta ,'.','color','b')

end